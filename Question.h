//
//  Question.h
//  QuizApp
//
//  Created by Ole on 03/02/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject

- (instancetype)initWithQuestion:(NSString *)question andAnswers:(NSArray *)answers;

- (NSString*)getQuestion;

- (NSArray*)getAnswers;


@end
