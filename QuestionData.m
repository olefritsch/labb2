//
//  QuestionData.m
//  QuizApp
//
//  Created by Ole on 04/02/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "QuestionData.h"
#import "Question.h"

@interface QuestionData()

@property (nonatomic) NSMutableArray *questions;

@end


@implementation QuestionData

- (instancetype)init {
    self = [super init];
    if (self) {
        self.questions = [[NSMutableArray alloc] init];
        Question *q01 = [[Question alloc] initWithQuestion:@"What is the highest grossing movie of all time?" andAnswers:@[@"Avatar", @"Titanic", @"The Force Awakens", @"Jurassic World"]];
        Question *q02 = [[Question alloc] initWithQuestion:@"Which of the following is a PS4 Exclusive title?" andAnswers:@[@"Until Dawn", @"Halo 4", @"Resistance 3", @"Little Big Planet"]];
        Question *q03 = [[Question alloc] initWithQuestion:@"Which of the following has NOT recieved more than 10 Oscars?" andAnswers:@[@"Slumdog Millionaire", @"Ben-Hur", @"Return of the King", @"Titanic"]];
        Question *q04 = [[Question alloc] initWithQuestion:@"Which album has sold most copies?" andAnswers:@[@"Thriller", @"Back in Black", @"Led Zeppelin IV", @"Pink Floyd"]];
        Question *q05 = [[Question alloc] initWithQuestion:@"Which of the following is not a vegetable?" andAnswers:@[@"Casserole", @"Fiddleheads", @"Fat hen", @"Tiger Nut"]];
        Question *q06 = [[Question alloc] initWithQuestion:@"What system is best for gaming?" andAnswers:@[@"PC", @"Xbos", @"Playstation", @"Doesn't matter"]];
        Question *q07 = [[Question alloc] initWithQuestion:@"What is the biggest eSports game?" andAnswers:@[@"Dota 2", @"Smite", @"League of Legends", @"World of Warcraft"]];
        Question *q08 = [[Question alloc] initWithQuestion:@"The great Victoria Desert is located in...?" andAnswers:@[@"Australia", @"West Africa", @"North Africa", @"South America"]];
        Question *q09 = [[Question alloc] initWithQuestion:@"The artic cirlce lies how many degrees above the equator?" andAnswers:@[@"66", @"60", @"70", @"72"]];
        Question *q10 = [[Question alloc] initWithQuestion:@"The Bermuda Triangle lies of the coast of..?" andAnswers:@[@"Florida", @"Japan", @"Hawaii", @"Australia"]];
        Question *q11 = [[Question alloc] initWithQuestion:@"Where are the Andes located?" andAnswers:@[@"South America", @"North America", @"Africa", @"Asia"]];
        Question *q12 = [[Question alloc] initWithQuestion:@"How many chess pieces does the chessboard have?" andAnswers:@[@"32", @"20", @"24", @"28"]];
        Question *q13 = [[Question alloc] initWithQuestion:@"Hearthstone is a..?" andAnswers:@[@"Card Game", @"Fantasy Game", @"Action Movie", @"RPG"]];
        Question *q14 = [[Question alloc] initWithQuestion:@"Which of the following is NOT amongst the 5 richest countries in the world?" andAnswers:@[@"USA", @"Norway", @"Qatar", @"Singapore"]];
        Question *q15 = [[Question alloc] initWithQuestion:@"What is the name of Super Mario's brother?" andAnswers:@[@"Luigi", @"Pizzaroni", @"Leonardo", @"Ferrari"]];
        Question *q16 = [[Question alloc] initWithQuestion:@"G.R.R Martin is responsible for the death of.." andAnswers:@[@"Eddard Stark", @"Martin Luther King", @"John F. Kennedy", @"Julius Caesar"]];
        Question *q17 = [[Question alloc] initWithQuestion:@"The Iron Throne rightfully belonged to ______ after Robert Baratheons death." andAnswers:@[@"His brother Stannis", @"His son Joffrey", @"His wife Cersei", @"His brother Renly"]];
        Question *q18 = [[Question alloc] initWithQuestion:@"Commander Shepard is famous for..?" andAnswers:@[@"Fighting Reapers", @"Defeating Napoleon", @"Invading China", @"Saving Private Ryan"]];
        Question *q19 = [[Question alloc] initWithQuestion:@"Why haven't White Walkers been seen for centuries?" andAnswers:@[@"They're sleeping", @"They're extinct", @"They dont exist", @"They're shy"]];
        Question *q20 = [[Question alloc] initWithQuestion:@"Why trait does FN-2187 not show?" andAnswers:@[@"Loyalty", @"Intelligence", @"Morality", @"Humor"]];
        Question *q21 = [[Question alloc] initWithQuestion:@"Who invented the car?" andAnswers:@[@"Carl Benz", @"Mercedes Jellinek", @"Dee Troit", @"Paul Car"]];
        
        [self.questions addObject:q01];
        [self.questions addObject:q02];
        [self.questions addObject:q03];
        [self.questions addObject:q04];
        [self.questions addObject:q05];
        [self.questions addObject:q06];
        [self.questions addObject:q07];
        [self.questions addObject:q08];
        [self.questions addObject:q09];
        [self.questions addObject:q10];
        [self.questions addObject:q11];
        [self.questions addObject:q12];
        [self.questions addObject:q13];
        [self.questions addObject:q14];
        [self.questions addObject:q15];
        [self.questions addObject:q16];
        [self.questions addObject:q17];
        [self.questions addObject:q18];
        [self.questions addObject:q19];
        [self.questions addObject:q20];
        [self.questions addObject:q21];
    }
    return self;
}

- (NSArray*)getAllQuestions {
    return self.questions;
}

@end
