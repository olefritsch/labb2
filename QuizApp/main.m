//
//  main.m
//  QuizApp
//
//  Created by Ole on 03/02/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
