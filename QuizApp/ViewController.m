//
//  ViewController.m
//  QuizApp
//
//  Created by Ole on 03/02/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "ViewController.h"
#import "Game.h"

@interface ViewController ()

@property (nonatomic) Game *game;
@property (nonatomic) NSMutableArray *buttonArray;
@property (nonatomic) BOOL playAgain;
@property (weak, nonatomic) IBOutlet UITextView *question;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *mistakeLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonOne;
@property (weak, nonatomic) IBOutlet UIButton *buttonTwo;
@property (weak, nonatomic) IBOutlet UIButton *buttonThree;
@property (weak, nonatomic) IBOutlet UIButton *buttonFour;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestionButton;

@end

@implementation ViewController

- (Game*)game {
    if (!_game) {
        _game = [[Game alloc] init];
    }
    return _game;
}

- (NSMutableArray*)buttonArray {
    if (!_buttonArray) {
        _buttonArray = [[NSMutableArray alloc] init];
        [_buttonArray addObject:self.buttonOne];
        [_buttonArray addObject:self.buttonTwo];
        [_buttonArray addObject:self.buttonThree];
        [_buttonArray addObject:self.buttonFour];
    }
    return _buttonArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES];
    [self.game startGame];
    [self setup];
    self.playAgain = NO;
}

- (void)resetUI {
    self.nextQuestionButton.hidden = YES;
    
    for (int i=0; i<self.buttonArray.count; i++) {
        UIButton *button = (UIButton*)self.buttonArray[i];
        if (button.tag == 1) {
            button.tag = 0;
            [button setTitleColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        }
        
        button.hidden = NO;
    }
}

- (void)setup {
    NSArray *questionBundle = [self.game nextQuestion];
    [self.question setText:questionBundle[0]];
    
    for (int i=0; i<4; i++) {
        [self.buttonArray[i] setTitle:questionBundle[i+1] forState:UIControlStateNormal];
    }
}

- (IBAction)guess:(UIButton *)sender {
    NSString *guess = [sender titleForState:UIControlStateNormal];
    [sender setTag:1];
    
    if ([self.game guessString:(guess)]) {
        [sender setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    } else {
        [sender setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
    
    for (int i=0; i<4; i++) {
        if ([self.buttonArray[i] tag] != 1) {
            ((UIButton*)self.buttonArray[i]).hidden = YES;
        }
    }
    
    self.nextQuestionButton.hidden = NO;
}

- (IBAction)nextQuestion:(UIButton *)sender {
    if (![self.game gameOver]) {
        [self setup];
        [self resetUI];
    } else if (self.playAgain){
        self.playAgain = NO;
        [self.game startGame];
        [self setup];
        self.scoreLabel.hidden = YES;
        self.mistakeLabel.hidden = YES;
        [self resetUI];
    } else {
        [self gameOver];
    }
}

- (void)gameOver {
    int score = [self.game getFinalScore];
    [self.question setText:@"GAME OVER"];
    [self.scoreLabel setText:[NSString stringWithFormat:@"You scored %d points", score]];
    [self.mistakeLabel setText:[NSString stringWithFormat:@"You made %d mistakes", (5-score)]];
    self.scoreLabel.hidden = NO;
    self.mistakeLabel.hidden = NO;
    
    for (int i=0; i<self.buttonArray.count; i++) {
        ((UIButton*)self.buttonArray[i]).hidden = YES;
    }
    
    [self.nextQuestionButton setTitle:@"Play again!" forState:UIControlStateNormal];
    
    self.playAgain = YES;
}




@end
