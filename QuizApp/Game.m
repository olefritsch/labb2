//
//  Game.m
//  QuizApp
//
//  Created by Ole on 03/02/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "Game.h"
#import "Question.h"
#import "QuestionData.h"

@interface Game()

@property (nonatomic) QuestionData *questionData;
@property (nonatomic) NSMutableArray *questionArray;
@property (nonatomic) NSMutableArray *currentQuestions;
@property (nonatomic) NSString *currentAnswer;
@property (nonatomic) int turn;
@property (nonatomic) int points;

@end

@implementation Game

- (instancetype)init {
    self = [super init];
    if (self) {
        self.questionData = [[QuestionData alloc] init];
        self.currentQuestions = [[NSMutableArray alloc] init];
        self.currentAnswer = [[NSString alloc] init];
    }
    return self;
}

- (void)startGame {
    self.turn = 0;
    self.points = 0;
    
    self.questionArray = [[self.questionData getAllQuestions] mutableCopy];
    [self.currentQuestions removeAllObjects];
    
    for (int i=0; i<=5; i++) {
        int index = arc4random() % self.questionArray.count;
        [self.currentQuestions addObject:self.questionArray[index]];
        [self.questionArray removeObjectAtIndex:index];
    }
}

- (BOOL)gameOver {
    return (self.turn >= 5) ? YES : NO;
}

- (NSArray *)nextQuestion {
    Question *question = self.currentQuestions[self.turn];
    NSMutableArray *questionBundle = [[NSMutableArray alloc] init];
    NSMutableArray *answers = [[question getAnswers] mutableCopy];

    self.currentAnswer = answers[0];

    [questionBundle addObject:[question getQuestion]];
    
    while (answers.count > 0) {
        int index = arc4random() % answers.count;
        [questionBundle addObject:answers[index]];
        [answers removeObjectAtIndex:index];
    }
    
    self.turn++;
    
    return questionBundle;
}


- (BOOL)guessString:(NSString *)guess {
    if ([guess isEqualToString:self.currentAnswer]) {
        self.points++;
        return YES;
    } else {
        return NO;
    }
}

- (int)getFinalScore {
    return self.points;
}

@end
