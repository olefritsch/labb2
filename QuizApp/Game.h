//
//  Game.h
//  QuizApp
//
//  Created by Ole on 03/02/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Game : NSObject

- (void)startGame;

- (BOOL)gameOver;

- (NSArray *)nextQuestion;

- (BOOL)guessString:(NSString *)guess;

- (int)getFinalScore;

@end
