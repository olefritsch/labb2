//
//  Question.m
//  QuizApp
//
//  Created by Ole on 03/02/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import "Question.h"

@interface Question()

@property (nonatomic) NSString *question;
@property (nonatomic) NSArray *answers;

@end

@implementation Question

- (instancetype)initWithQuestion:(NSString *)question andAnswers:(NSArray *)answers {
    self = [super init];
    if (self) {
        self.question = question;
        self.answers = answers;
    }
    return self;
}

- (NSString*)getQuestion {
    return self.question;
}

- (NSArray*)getAnswers {
    return self.answers;
}


@end
