//
//  QuestionData.h
//  QuizApp
//
//  Created by Ole on 04/02/16.
//  Copyright © 2016 Ole Fritsch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionData : NSObject

- (NSArray*)getAllQuestions;

@end
